#!/bin/bash

set -e

INSTALL_DIR=$HOME/.fonts/c/

mkdir -p $INSTALL_DIR

cp $PWD/fonts/*.ttf $INSTALL_DIR
echo "Done!"
