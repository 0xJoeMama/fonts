# Windows Fonts for Linux Installations

Installing the fonts isn't that difficult, but if you want to do it automagically just follow the instructions below.

## Installation 
Follow the steps listed below to install the fonts:
* Clone this repo
* `cd` into it
* Run `chmod +x install.sh`
* Finish off by running the script using `./install.sh`

You are done. Congratulations you now are the proud owner of some Microsoft Windows specific fonts, without having to rely on AUR packages or anything else.  

All you need to do to finish this off, is to just run `fc-cache` and you are fully done. However I would also recommend logging out and back in to your desktop environment/window manager.

